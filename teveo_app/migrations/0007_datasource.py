# Generated by Django 5.0.3 on 2024-04-28 08:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("teveo_app", "0006_camera_num_comements"),
    ]

    operations = [
        migrations.CreateModel(
            name="DataSource",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=200)),
                ("file_path", models.CharField(max_length=200)),
            ],
        ),
    ]

from django.apps import AppConfig


class TeveoAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "teveo_app"
